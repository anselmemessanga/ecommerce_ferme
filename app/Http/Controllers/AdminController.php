<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function dashboard(){
        return view('admin.dashboard');
    }

    public function commandes(){

        $orders = Order::get();

        $orders->transform(function($order, $key){
            $order->panier = unserialize($order->panier);

            return $order;
        });

        return view('admin.commandes')->with('orders', $orders);
    }

    public function creer_admin_compte(Request $request){
        $this->validate($request, ['email' =>'email|required|unique:admins',
                                   'password' => 'required|min:4']);

        $client = new Admin();
        $client->email = $request->input('email');
        $client->password = bcrypt($request->input('password'));

        $client->save();

        return back()->with('status', 'Votre compte a été créer avec succès !');
    }

    public function acceder_admin_compte(Request $request){
        $this->validate($request, ['email' =>'email|required',
                                   'password' => 'required']);

        $admin = Admin::where('email', $request->input('email'))->first();

        if($admin){
            if (Hash::check($request->input('password'), $admin->password)) {
                # code...
                Session::put('admin', $admin);
                return redirect('/admin');
            } else {
                # code...
                return back()->with('status', 'Mauvais mot de passe de passe ou email');
            }

        }
        else{
            return back()->with('status', "Vous n'avez pas de compte");
        }
    }

    public function admin_login(){
        return view('admin.login');
    }

    public function admin_signup(){
        return view('admin.signup');
    }

}
